package br.com.codenation.desafioexe;

import java.util.ArrayList;
import java.util.List;

public class DesafioApplication {

	public static List<Integer> fibonacci() {
		List<Integer> fibonacciN = new ArrayList<Integer>();
		Integer ultimo = 0;
		Integer anterior = 1;
		Integer x = 0;
		fibonacciN.add(ultimo);
		fibonacciN.add(anterior);
		while( x <= 350){
			x = ultimo + anterior;
			ultimo = anterior;
			anterior = x;
			fibonacciN.add(x);
		}
		return fibonacciN;
	}

	public static Boolean isFibonacci(Integer a) {
		List<Integer> seqFibonacci = fibonacci();
		return seqFibonacci.contains(a);
	}

}