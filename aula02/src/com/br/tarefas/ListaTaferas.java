package com.br.tarefas;

import sun.awt.X11.XSystemTrayPeer;

import java.util.ArrayList;

public class ListaTaferas {

    ArrayList<Tarefas> tarefas = new ArrayList<>();

    public void add(Tarefas job){
        if(job != null && job.ContaTamanhoDaTarefa()<= 20){
            this.tarefas.add(job);
        }else{
            System.out.println("Tarefa inválida");
        }

    }

    public void add (String descricao){
        Tarefas tarefa = new Tarefas(descricao);
        add(tarefa);
    }

    public void remover(int posicao){
        if(posicao < this.tarefas.size()){
            this.tarefas.remove(posicao);
        }else{
            System.out.println("A tarefa não existe");
        }

    }

    public Tarefas buscar (String descricao){
        for(Tarefas tarefa : tarefas){
            if(descricao.equals(tarefa.descricao)){
                return tarefa;
            }
        }
        System.out.println("Tarefa não encontrada ");
        return null;
    }

    public void remover(Tarefas tarefa){
        this.tarefas.remove(tarefa);
    }

    public void exibirTarefas(){
        for(Tarefas tarefa:tarefas){
            tarefa.ExibirTarefa();
        }
    }

}
