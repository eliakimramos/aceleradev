package com.br.tarefas;

public class Tarefas {
    String descricao;

    public Tarefas(String descricao){
        this.descricao = descricao;
    }
    public void ExibirTarefa(){
        System.out.println(descricao);
    }

    public  int ContaTamanhoDaTarefa(){
        return this.descricao.length();
    }
}
