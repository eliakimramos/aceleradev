import com.br.tarefas.ListaTaferas;
import com.br.tarefas.Tarefas;

public class Principal {

    public static void main(String[] args) {

        ListaTaferas listatarefas = new ListaTaferas();
        Tarefas t3 = new Tarefas("Me Exercitar");
        listatarefas.add("Estudar Java");
        listatarefas.add("Estudar Angular");
        listatarefas.add("Me Exercitar");
        listatarefas.add("Trabalhar");

        System.out.println("Minhas tarefas");
        System.out.println("----------------------------");
        listatarefas.exibirTarefas();
        System.out.println("\n");

        listatarefas.remover(t3);

        System.out.println("Minhas tarefas depois de remover");
        System.out.println("----------------------------");
        listatarefas.exibirTarefas();
        System.out.println("\n");


        System.out.println("Buscar tarefa ");
        System.out.println("----------------------------");
        Tarefas tarefaEncontrada = listatarefas.buscar("Estudar Java");
        tarefaEncontrada.ExibirTarefa();
        System.out.println("\n");
    }

}
