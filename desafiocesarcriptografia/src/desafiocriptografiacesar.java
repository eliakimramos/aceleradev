public class desafiocriptografiacesar {


    public String criptografar(String texto) {
        String criptografado ="";
        char ascii = 0;
        char x, y;
        int chave = 3;
        while (chave >= 26) {//chave tem que ter o tamanho do alfabeto
            chave = chave - 26;
        }
        for (int i = 0; i < texto.length(); i++) {
            //Tratamento Letras minusculas
            if (texto.charAt(i) >= 97 && texto.charAt(i) <= 122) {//letrans minusculas de acordo com a tabela ASCII
                if ((int) (texto.charAt(i) + chave) > 122) {
                    x = (char) (texto.charAt(i) + chave);
                    y = (char) (x - 122);
                    ascii = (char) (96 + y);
                } else {
                    ascii = (char) (texto.charAt(i) + chave);
                }
            }
            if(texto.charAt(i) == 32){
                ascii = (char) (texto.charAt(i));
            }
            if(texto.charAt(i) >= 33 && texto.charAt(i) <= 64){
                ascii = (char) (texto.charAt(i));
            }
            if(texto.charAt(i) >= 91 && texto.charAt(i) <= 96){
                ascii = (char) (texto.charAt(i));
            }
            //Tratamento Letras maiusculas
            if (texto.charAt(i) >= 65 && texto.charAt(i) <= 90) {
                if (texto.charAt(i) + chave > 90) {
                    x = (char) (texto.charAt(i) + chave);
                    y = (char) (x - 90);
                    ascii = (char) (64 + y);
                } else {
                    ascii = (char) (texto.charAt(i) + chave);
                }
            }
            criptografado+= Character.toLowerCase(ascii);
        }
        if(criptografado == null || criptografado == ""){
            throw new UnsupportedOperationException("error criptografia está vazia");
        }
        return criptografado.toLowerCase();
    }
    
    public String descriptografar(String texto) {
        String descriptografado ="";
        char ascii = 0;
        char x, y;
        int chave = 3;

        while (chave >= 26) {//chave tem que ter o tamanho do alfabeto
            chave = chave - 26;
        }
        for (int i = 0; i < texto.length(); i++) {
            //Tratamento Letras minusculas
            if (texto.charAt(i) >= 97 && texto.charAt(i) <= 122) {//letrans minusculas de acordo com a tabela ASCII
                if ((int) (texto.charAt(i) - chave) > 122) {
                    x = (char) (texto.charAt(i) - chave);
                    y = (char) (x - 122);
                    ascii = (char) (96 - y);
                } else {
                    ascii = (char) (texto.charAt(i) - chave);
                }
            }
            if(texto.charAt(i) == 32){
                ascii = (char) (texto.charAt(i));
            }
            if(texto.charAt(i) >= 33 && texto.charAt(i) <= 64){
                ascii = (char) (texto.charAt(i));
            }
            if(texto.charAt(i) >= 91 && texto.charAt(i) <= 96){
                ascii = (char) (texto.charAt(i));
            }
            //Tratamento Letras mausculas
            if (texto.charAt(i) >= 65 && texto.charAt(i) <= 90) {
                if (texto.charAt(i) - chave > 90) {
                    x = (char) (chave - texto.charAt(i));
                    y = (char) (x - 90);
                    ascii = (char) (64 - y);
                } else {
                    ascii = (char) (chave - texto.charAt(i));
                }
            }
            descriptografado += Character.toLowerCase(ascii);
        }
        if(descriptografado == null ){
            throw new UnsupportedOperationException("error descriptografia está vazia");
        }if(descriptografado ==  ""){
            throw new IllegalArgumentException("error descriptografia está vazia");
        }
        return descriptografado.toLowerCase();
        //throw new UnsupportedOperationException("esse method nao esta implementado aainda");
    }

}
