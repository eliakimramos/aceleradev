import java.util.Arrays;

public class CalculaSalario {

    public long calcularSalarioLiquido(double salarioBase) {
        //Use o Math.round apenas no final do método para arredondar o valor final.
        //Documentação do método: https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html#round-double-
        double resultado = 0.0;
        double resultadoInss = 0.0;
        if(salarioBase > 1039.00){
            resultadoInss = calcularInss(salarioBase);
            resultado = calcularIRRF(resultadoInss);
        }
        return Math.round(resultado);
    }


    //Exemplo de método que pode ser criado para separar melhor as responsábilidades de seu algorítmo
    private double calcularInss(double salarioBase) {
        double resultado = 0.0;
        if(salarioBase <= 1500){
            resultado = salarioBase - (salarioBase * 0.08);
        }else if(salarioBase > 1500 && salarioBase <= 4000){
            resultado = salarioBase - (salarioBase*0.09);
            System.out.println(resultado);
        }else if(salarioBase > 4000){
            resultado = salarioBase - (salarioBase*0.11);
            System.out.println(resultado);
        }

        return resultado;
    }

    private double calcularIRRF(double salarioBase) {
        double resultado = 0.0;
        if(salarioBase <= 3000){
            resultado = salarioBase;
        }else if(salarioBase > 3000 && salarioBase <= 6000){
            System.out.println(salarioBase);
            resultado = salarioBase - (salarioBase*0.075);
            System.out.println(resultado);
        }else if(salarioBase > 6000){
            resultado = salarioBase - (salarioBase*0.15);
        }

        return  resultado;
    }

    public static void main(String[] args) {
        CalculaSalario calculadora = new CalculaSalario();
        long resultado = calculadora.calcularSalarioLiquido(4000.01);
        System.out.println(resultado);
    }
}
